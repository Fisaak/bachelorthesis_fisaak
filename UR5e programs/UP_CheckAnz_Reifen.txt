 Programm
   Roboterprogramm
     FahreAchse
       Wegpunkt_1
     Einstellen AktAnzReifen=0
     Cam locate
       Für das nächste Objekt
         var_AnzReifen≔f[0]
         Warten: 5.0
         If var_AnzReifen≥4
           'MiR mitteilen da genug Reifen für Montage vorhanden sind'
           'Set MiR register PLC register 20 to 4.0'
           Einstellen AktAnzReifen=var_AnzReifen
         ElseIf var_AnzReifen<4
           'var_aktReifenAn≔f[0]'
           'Set MiR register PLC register 20 to 1.0'
           Einstellen AktAnzReifen=var_AnzReifen
       Wenn kein Objekt gefunden wurde
         'Optional'
         'Set MiR register PLC register 20 to 1.0'
         var_AnzReifen≔0
     Home
     Halt
