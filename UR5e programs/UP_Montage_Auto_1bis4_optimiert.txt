 Programm
   VorStart
     var_Zaehler≔0
   Roboterprogramm
     FahreLinear
       P99
     Schleife 4 mal
       'Schauen ob 4 Reifen vorhanden sind'
       Cam locate
         Für das nächste Objekt
           'Reifen holen'
           var_AnzReifen≔f[0]
           FahreLinear
             'Reifen entnehmen'
             VorPos1_1
             EntPos1_1
             Warten: 0.5
             Greifer geschlossen (1)
             Warten: 0.5
             ZwPos1_2
           var_Zaehler≔var_Zaehler+1
         Wenn kein Objekt gefunden wurde
           'keine Reifen vorhanden. Bitte Reifen einlegen'
       Switch var_Zaehler
         Case 1
           FahreLinear
             'Zwischenweg zu Montage Position 1'
             VorPos2
             VorPos3
             FahreLinear
               'Reifen an Auto Platz 1 montieren'
               VorPos1Mon
               ZwPos1Mon
               MonPos1
               Warten: 0.3
               Greifer offen (1)
               Warten: 0.3
               VorPos3
               FahreAchse
                 'Rückweg Richtung Entnahme 2. Reifen'
                 RückPos_1_2
                 VorPos2
         Case 2
           FahreLinear
             'Zwischenweg zu Montage Position 2'
             VorPos2
             VorPos3
             FahreLinear
               'Reifen an Auto Platz 2 montieren'
               VorPos2Mon
               ZwPos2Mon
               MonPos2
               Warten: 0.3
               Greifer offen (1)
               Warten: 0.3
               ZwPos2Mon
               FahreAchse
                 'Rückweg Richtung Entnahme 3. Reifen'
                 RückPos_1_2
                 VorPos2
         Case 3
           FahreLinear
             'Zwischenweg zu Montage Position 3'
             VorPos4
             VorPos5
             FahreLinear
               'Reifen an Auto Platz 3 montieren'
               VorPos3_Mon
               ZwPos3Mon
               MonPos3
               Warten: 0.3
               Greifer offen (1)
               Warten: 0.3
               ZwPos3Mon_1
               FahreAchse
                 'Rückweg Richtung Entnahme 4. Reifen'
                 RueckPos_3_4
         Case 4
           FahreLinear
             'Zwischenweg zu Montage Position 4'
             VorPos4
             VorPos5
             FahreLinear
               'Reifen an Auto Platz 4 montieren'
               VorPos4Mon
               ZwPos4Mon
               MonPos4_1
               Warten: 0.3
               Greifer offen (1)
               Warten: 0.3
               ZwPos4Mon_1
               FahreAchse
                 'Rückweg Richtung Snapshotpos für QS-Kontrolle'
                 RueckPos_3_4
     'Endkontrolle ob Reifen korrekt montiert wurden'
     Cam locate
       Für gefundene Objekte
         Einstellen AutoKorrMon=10
       Wenn kein Objekt gefunden wurde
         'Optional'
         Einstellen AutoKorrMon=111
     Home
     Halt
