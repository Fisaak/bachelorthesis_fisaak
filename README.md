Infos Projektordner:

-----------------------------------------------------------------------

-Die Datei MiR100.exe kann unter Windows 10 und 11 gestartet werden
und stellt dem Nutzer eine Grafische Nutzeroberfläche zum Starten 
einer gewünschten MiR100 Mission.

-Um das Python Projekt nutzen bzw. abändern zu können kann das Projekt 
mittels PyCharm geöffnet werden.

-Die URP-Dateien im Ordner "UR-Programme_101122" können per 7-Zip 
eingesehen werden und enthalten XML-Dateien.

-Die PAP-Datei kann online unter: https://app.diagrams.net/ oder mit 
der App Drawio geöfnnet werden (https://drawio-app.com/).


Author: Felix Isaak
Datum: 05.12.22

----------------------------------------------------------------------